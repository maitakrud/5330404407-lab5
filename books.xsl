<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:lib="http://www.zvon.org/library">
    <xsl:template match="rdf:RDF">
        <html>
            <body>
                <table border="1">
                    <tr>
                        <th>Title</th>
                        <th>Pages</th>
                    </tr>
                    <xsl:for-each select="rdf:Description[lib:pages]">
                        <xsl:sort select="lib:pages"/>
                        <tr>
                            <td>
                                <xsl:value-of select="@about"/>
                            </td>
                            <td>
                                <xsl:value-of select="lib:pages"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>