<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:atom="http://www.w3.org/2005/Atom" 
                xmlns:georss="http://www.georss.org/georss" 
                xmlns:twitter="http://api.twitter.com">
    <xsl:template match="rss">
        <html>
            <body>
                <h1>Twitter @
                    <a>
                        <xsl:attribute name = "href">
                            <xsl:value-of select="channel/link"/>
                        </xsl:attribute>
                        <xsl:value-of select="channel/link"/>
                    </a>
                </h1>
                <table border="1">
                    <tr>
                        <th width="30">Title</th>
                        <th width="20">Publication Date</th>
                    </tr>
                    <xsl:for-each select="channel/item">
                        <tr>
                            <td>
                                <a>
                                    <xsl:attribute name = "href">
                                        <xsl:value-of select="link"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="title"/>
                                </a>
                            </td>
                            <td>
                                <xsl:value-of select="pubDate"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>