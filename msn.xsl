<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="Log">
        <html>
            <head>
                <META http-equiv="Content-Type" content ="text/html; charset=UTF-8"/> 
                <meta http-equiv="Content-Type" content ="text/html; charset=utf-8"/>
                <title>MSN Log</title>
                <style type="text/css">
                    body
                    {
                        font-family: Verdana, arial, sans-serif;
                    }

                </style>
            </head>
            <body>
                <span style="color: red" >
                        [Conversation start on <xsl:value-of select="Message/@Date"/> " " <xsl:value-of select="Message/@Time"/>
                </span>
                <table border="0">
                    <xsl:for-each select="Message">
                            <xsl:choose>
                                <xsl:when test="From/User/@FriendlyName=&quot;Mana&quot;">
                                    <tr><td> [ <xsl:value-of select="@Date"/> <xsl:value-of select="@Time"/> ] </td>
                                    <td><span style="color: #FFAA00">Mana</span></td><td>:</td>
                                        <td><span style="color: #FFAA00"><xsl:value-of select="Text"/></span></td></tr>
                                </xsl:when> 
                                <xsl:otherwise>
                                    <tr><td> [ <xsl:value-of select="@Date"/> <xsl:value-of select="@Time"/> ] </td>
                                    <td><span style="color: #24913c">Manee</span></td><td>:</td>
                                        <td><span style="color: #24913c"><xsl:value-of select="Text"/></span></td></tr>
                                </xsl:otherwise>
                                </xsl:choose>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>