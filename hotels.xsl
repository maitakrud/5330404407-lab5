<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:ns="http://www.opengis.net/kml/2.2"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:atom="http://www.w3.org/2005/Atom">
    <xsl:template match="ns:kml/ns:Document">
        <html>
            <h1>
                <img>
                    <xsl:attribute name = "src">
                        <xsl:value-of select="ns:Style/ns:IconStyle/ns:Icon/ns:href"/>
                    </xsl:attribute>
                    <xsl:value-of select="ns:name"/>
                </img>
            </h1>
            <p>
                <xsl:value-of select="substring-before(ns:description, ',')"/>
                <ul>
                    <xsl:for-each select="ns:Placemark">
                        <xsl:sort select="ns:name"/>
                        <li>
                            <xsl:value-of select="ns:name"/>
                            <ul>
                                <li>
                                    <a>
                                        <xsl:attribute name = "href">
                                            <xsl:value-of select="substring-before(substring-after(ns:description, '&quot;'), '&quot;')"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="substring-before(substring-after(ns:description, '&quot;'), '&quot;')"/>
                                    </a>
                                </li>
                                <li>
                                    Coordinates:
                                    <xsl:value-of select="ns:Point/ns:coordinates"/>
                                </li>
                            </ul>
                        </li>
                    </xsl:for-each>
                </ul>
            </p>
        </html>
    </xsl:template>
</xsl:stylesheet>